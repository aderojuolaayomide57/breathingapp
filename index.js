/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AnimateApp from './App';
import {name as appName} from './app.json';
//import '@flyskywhy/react-native-browser-polyfill';

AppRegistry.registerComponent(appName, () => AnimateApp);
