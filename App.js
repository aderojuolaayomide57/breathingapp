/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import App from './src';
 
 
 class AnimateApp extends React.Component{
 
   render() {
     return (
         <App />
     )
   }
 
 }
 
 export default AnimateApp;
 