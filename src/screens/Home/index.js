import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Alert } from 'react-native';




const Home = (props) => {
    const { navigation: { navigate } } = props;
    return(
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigate('Ellipse')} style={styles.wrapper}>
                    <Text style={styles.menu}>Ellipse</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('WaterWave')} style={styles.wrapper}>
                    <Text style={styles.menu}>WaterWave</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Particles')} style={styles.wrapper}>
                    <Text style={styles.menu}>Particles</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('StressRelease')} style={styles.wrapper}>
                    <Text style={styles.menu}>Stress Release</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('StressReleaseCircle')} style={styles.wrapper}>
                    <Text style={styles.menu}>Stress Release Circle</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Circle')} style={styles.wrapper}>
                    <Text style={styles.menu}>Circle</Text>
            </TouchableOpacity>
        </View>
    )
}


export default Home;



const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        paddingTop: 50
    },
    menu: {
        fontSize: 20,
        color: '#3681cc',
        fontWeight: 'bold'
    },
    wrapper: {
        padding: 20
    }
});