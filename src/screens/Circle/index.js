import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';

import Ball from './ball';

class Circle extends Component {
    render(){
        return (
            <View style={styles.container}>
                <Ball 
                  delay={100}// delay time for each ball
                  positionx={100} // position x of the moving ball
                  positiony={100} // position y of the moving ball
                  x={0} // initial starting point x position of the ball 
                  y={0} // initial starting point y position of the ball
                  color="green" // color of the ball 
                />
                <Ball 
                  delay={500} 
                  positionx={-120}
                  positiony={-120}
                  x={0}
                  y={5}
                  color="red"
                />
                <Ball 
                  delay={1000} 
                  positionx={0}
                  positiony={-150}
                  x={5}
                  y={0}
                  color="blue"
                />
                <Ball 
                  delay={1200} 
                  positionx={-100}
                  positiony={100}
                  x={0}
                  y={-5}
                  color="purple"
                />
                <Ball 
                  delay={1200} 
                  positionx={100}
                  positiony={-100}
                  x={0}
                  y={-5}
                  color="pink"
                />
                <Ball 
                  delay={1500} 
                  positionx={0}
                  positiony={150}
                  x={-5}
                  y={0}
                  color="cyan"
                />
                <Ball 
                  delay={1700} 
                  positionx={150}
                  positiony={0}
                  x={-5}
                  y={-5}
                  color="yellow"
                />
                <Ball 
                  delay={1700} 
                  positionx={-150}
                  positiony={0}
                  x={-5}
                  y={-5}
                  color="orange"
                />
            </View>
        );      
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


export default Circle;