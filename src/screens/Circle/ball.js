import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Animated, 
    Easing,
    TouchableOpacity,
    Text,
    Dimensions,
    interpolate,
    Alert,
 } from 'react-native';
  

class Ball extends Component{

    constructor(props){
        super(props);

        this.state = {
            positionXY: new Animated.ValueXY(0, props.y),
            sizeXY: new Animated.Value(70), // initial size for each ball
            endValue: 0, // ending value size for each ball
            duration: 4000, // duration time for the ball
        }
    } 



    componentDidMount() {
        Animated.loop(
            Animated.sequence([
                Animated.parallel([
                    Animated.timing(this.state.sizeXY, {
                        delay: this.props.delay, // delay time specified for each ball 
                        toValue: this.state.endValue,
                        duration: this.state.duration,
                        useNativeDriver: true,
                    }),
                    Animated.timing(this.state.positionXY, {
                        delay: this.props.delay,
                        toValue: {x: this.props.positionx, y: this.props.positiony}, // to position value x and y specified for each ball
                        duration: 3000, // duration time for the ball to move to the point specified 
                        useNativeDriver: true,
                    })
                ]),
                Animated.parallel([
                    Animated.timing(this.state.positionXY, {
                        delay: 2000, // delay time for the ball to move back to start moving back to it initial position
                        toValue: 0, // x and why position will move bact to the initial position of zero
                        duration: 3000, // time taken to move back to the initial position
                        useNativeDriver: true,
                    }),
                    Animated.timing(this.state.sizeXY, {
                        delay: 2500, // delay time for the ball to start increase back to initial size
                        toValue: 70, // initial size of the ball 70
                        duration: 4000, // total time duration for the ball to increase back to the initial size
                        useNativeDriver: true,
                    })
                ])
            ])
        ).start();
    }

     
  
    
    render(){
        
        return (
            <Animated.View style={[
                    styles.ring, 
                    this.props.x > 0 && {marginLeft: this.props.x},
                    {backgroundColor: this.props.color},
                    {
                        transform: [
                          {
                            translateX: this.state.positionXY.x,
                            translateY: this.state.positionXY.y,
                            scale: this.state.sizeXY,
                          },
                        ],
                    },                
                ]} 
            />
        );
    }
};


const styles = StyleSheet.create({
    ring: {
      borderRadius: 50,
      position: "absolute",
      height: 1,
      width: 1,
    },
});
  


export default Ball;
