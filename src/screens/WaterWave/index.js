import React, { useEffect, useRef, useState } from "react";
import {
   View, 
   Dimensions,
   StyleSheet,
   Image,
 } from 'react-native';


 import Animated, {
  useSharedValue,
  Easing,
  useAnimatedProps,
  useDerivedValue,
  withSpring,
  withTiming,
  withRepeat,
  interpolate,
  withDelay
} from "react-native-reanimated";

import MaskedView from '@react-native-community/masked-view';
import Svg, { Path, Circle } from 'react-native-svg';


//import {mix} from './utils/helper';
  
  
  const SIZE = Dimensions.get('window').width - 70;
  const AnimatedPath = Animated.createAnimatedComponent(Path);
  const AnimatedCircle = Animated.createAnimatedComponent(Circle);

  
  
  

const WaterWave = () => {



  const progress = useSharedValue(0);


  useEffect(() => {
    progress.value = withRepeat(
      withTiming(1, {
        duration: 4000,
        easing: Easing.inOut(Easing.ease)
      }),
      -1,
      true
    );
  }, []);

  const inputRange = [0, 1]
   

  const data = useDerivedValue(() => {

    return {
      from: {x: interpolate(progress.value, inputRange, [-1, -1]), y: interpolate(progress.value, inputRange, [0.2, 0.5])},
      c1: {x: interpolate(progress.value, inputRange, [0.5, 0]), y: interpolate(progress.value, inputRange, [0.3, 0.5])},
      c2: {x: interpolate(progress.value, inputRange, [0.5, 1]), y: interpolate(progress.value, inputRange, [0.7, 0.5])},
      to: {x: interpolate(progress.value, inputRange, [1.1, 2]), y: interpolate(progress.value, inputRange, [0.7, 0])},

    }
  })

  const data2 = useDerivedValue(() => {

    return {
      from: {x: interpolate(1-progress.value, inputRange, [-1, -1]), y: interpolate(1-progress.value, inputRange, [0.3, 1])},
      c1: {x: interpolate(1-progress.value, inputRange, [0.5, 0]), y: interpolate(1-progress.value, inputRange, [0.3, 0.5])},
      c2: {x: interpolate(1-progress.value, inputRange, [0.5, 1]), y: interpolate(1-progress.value, inputRange, [0.7, 0.5])},
      to: {x: interpolate(1-progress.value, inputRange, [1.1, 2]), y: interpolate(1-progress.value, inputRange, [0.7, 0])},

    }
  })

  const path = useAnimatedProps(() => {
    const {from, c1, c2, to} = data.value;
    return {
      d: `M ${from.x} ${from.y} C ${c1.x} ${c1.y} ${c2.x} ${c2.y} ${to.x} ${to.y} L 1 1 L 0 1 Z`,
    }
  })

  const path2 = useAnimatedProps(() => {
    const {from, c1, c2, to} = data2.value;
    return {
      d: `M ${from.x} ${from.y} C ${c1.x} ${c1.y} ${c2.x} ${c2.y} ${to.x} ${to.y} L 1 1 L 0 1 Z`,
    }
  })

  

  
    
    return (
      <View
        style={styles.container}
      >

        <MaskedView
          maskElement={
            <View
              style={styles.maskview}
            />
          } 
        >
          <Svg width={SIZE} style={{backgroundColor: '242424'}} height={SIZE} viewBox="0 0 1 1">
            <AnimatedPath fill="#3681cc" animatedProps={path}/>
          </Svg>
        </MaskedView>
      </View>
    );
  }
  
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'black',
    },
    svg: { backgroundColor: '#242424', zIndex: 1 },
    text: {
      fontSize: 30,
      color: '#ffffff',
      zIndex: 10,
    },
    maskview: { flex: 1, 
      backgroundColor: 'black', 
      width: SIZE, 
      height: SIZE, 
      borderRadius: 30 
    }

  });
  
  
  
  export default WaterWave;
  