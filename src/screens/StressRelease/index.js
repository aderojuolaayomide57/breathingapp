import React, { Component } from 'react';
import { View } from 'react-native';
import ParticlesBg from 'react-native-particles-bg'

class StressRelease extends Component {

  render () {
    let config = {
        num: [5, 10],
        rps: 0.2,
        radius: [20, 40],
        life: [1.5, 3],
        v: [2, 3],
        tha: [-20, 40],
        // body: "./img/icon.png", // Whether to render pictures
        // rotate: [0, 20],
        alpha: [0.6, 1],
        scale: [0.8, 2],
        position: "center", // all or center or {x:1,y:1,width:100,height:100}
        color: ["random", "#ff0000"],
        bround: "dead", // cross or bround
        random: 15,  // or null,
        g: 0,    // gravity
        f: [1, -2], // force
        onParticleUpdate: (ctx, particle) => {
            ctx.beginPath();
            ctx.rect(particle.p.x, particle.p.y, particle.radius / 2, particle.radius / 2);
            ctx.fillStyle = particle.color;
            ctx.fill();
            ctx.closePath();
        }
      };
  
      return (
        <View style={{width: "100%", height :400, flex: 1, backgroundColor: 'black'}}>
          <ParticlesBg type="custom" config={config} bg={true} />
        </View>
      );
  }
}

export default StressRelease;