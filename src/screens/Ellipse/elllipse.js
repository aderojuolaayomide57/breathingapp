/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { Component } from 'react';
 import {
    StyleSheet,
    View,
    Animated, 
    Easing,
    TouchableOpacity,
    Text
 } from 'react-native';
 
 
import EllipseWithTimer from './components/';
import EllipseWithoutTimer from './components/index2';




 
 const EllipseTimer = (props) => {
    const {values} = props.route.params
     return (
         <View style={styles.container}>
             {(values.delay_bottom === 0) &&
                <EllipseWithoutTimer
                    values={values}
                />
             }
             {(values.delay_bottom > 0) &&
                <EllipseWithTimer
                    values={values}
                />
             }
         </View>
     )
 }

 export default EllipseTimer
 

 const styles = StyleSheet.create({
 container: {
     flex: 1,
 },

});