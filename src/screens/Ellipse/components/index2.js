/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { Component } from 'react';
 import {
   StyleSheet,
   View,
   Animated, 
   Easing,
  TouchableOpacity,
  Text,
  Dimensions
 } from 'react-native';
 
 
 import Ball from '../../../assets/ball.svg';
 import WaveLongBottom from '../../../assets/WaveLongBottom.svg';
 import WaveLongMiddle from '../../../assets/WaveLongMiddle.svg';

 //import Wave1 from '../../../assets/wave1.svg';
 //import Wave2 from '../../../assets/wave2.svg';


 const { width } = Dimensions.get("window");
 const { height } = Dimensions.get("window");


 
 
 export default class EllipseWithoutTimer extends Component {
   constructor(props) {
     super(props);
     this.animated = new Animated.Value(0);
     this.animateBg = new Animated.Value(300);
     this.animateBg2 = new Animated.Value(-300);
     var inputRange = [0, 2];
     var outputRange = ['180deg', '540deg'];
     this.rotate = this.animated.interpolate({inputRange, outputRange});

 }
 
 resetAnimation(){
  this.animated.setValue(-1);
}

 playAnimation(){
  this.animated.setValue(0);
  this.animate();
  if(this.props.values.overall_time > 0){
    setTimeout(
      () => {
        this.animated.stopAnimation()
        this.animated.setValue(0);
      }, 
      this.props.values.overall_time * 60000);
  }
}


 stopAnimation(){
  this.animated.stopAnimation();
  this.animated.setValue(0);

  }
 
 animate() {
   Animated.loop(
     Animated.timing(this.animated, {
         toValue: 2,
         delay: this.props.values.delay_top <= 0 ? 1000 : this.props.values.delay_top * 2000,
         duration: this.props.values.inhale <= 0 ? 10000 : this.props.values.inhale * 2000,
         useNativeDriver: true,
         easing: Easing.in(Easing.ease)
        }),
   ).start();

   Animated.loop(
      Animated.timing(this.animateBg, {
        toValue: -80,
        duration: 5000,
        useNativeDriver: true,
        easing: Easing.inOut(Easing.ease)
      })
    ).start();

    Animated.loop(
      Animated.timing(this.animateBg2, {
        toValue: 380,
        duration: 5000,
        useNativeDriver: true,
        easing: Easing.inOut(Easing.ease)
      })
    ).start();
 }

 
 render() {

    
     const transform = [{rotate: this.rotate}];
     const transformBg = [{ translateX: this.animateBg }]
     const transformBg2 = [{ translateX: this.animateBg2 }]
     

     return (
       <View style={styles.container}>
          <View style={styles.topContainer}>
              <TouchableOpacity 
                style={styles.btnContainer}
                onPress={() => this.playAnimation()}
              >
                <Text style={styles.textStyle}>Play</Text>
              </TouchableOpacity>
              
              <TouchableOpacity 
                style={styles.btnContainer}
                onPress={() => this.stopAnimation()}
              >
                <Text style={styles.textStyle}>Stop</Text>
              </TouchableOpacity>

              <TouchableOpacity 
                style={styles.btnContainer}
                onPress={() => this.resetAnimation()}
              >
                <Text style={styles.textStyle}>Reset</Text>
              </TouchableOpacity>
          </View>
         <Animated.View style={[styles.wave, {transform: transformBg}]}>
              <WaveLongMiddle 
                  height={350} 
                  
              />
          </Animated.View>
          <Animated.View style={[{transform: transformBg2}]}>
              <WaveLongBottom 
                  height={320} 
                  style={styles.wave2}
              />
          </Animated.View>
          <View style={styles.circleBox}>

          </View>
         <Animated.View style={[styles.item, {transform}]}>
           <Animated.View style={[styles.dot]}>
             <Ball width={120} height={40} />
           </Animated.View>
         </Animated.View>
       </View>
     );
  }
 }

 const styles = StyleSheet.create({
 container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
     position: 'relative',
     backgroundColor: '#3681cc',

 },
 item: {
      position: 'absolute',
      width: 290,
      height: 290,
      //width: width * 0.32,
      //height: height * 0.30, // this is the diameter of circle
      transform: [
        {
          scaleY: 1,
        },
    ],
 },
 dot: {
     width: '100%',
     height: 20,
     position: 'absolute',
     alignItems: 'center',
     justifyContent: 'center',
 },

  circleBox: {
    borderRadius: 150,
    borderWidth: 1,
    borderColor: 'white',
    //width: width * 0.60,
    //height: height * 0.28,
    width: 280,
    height: 280,
    position: 'absolute',
  },
  wave: {
    marginTop: -(height * 0.15),
    overflow: 'hidden',
    position: 'absolute',

  },
  wave2: {
      marginVertical: -(height * 0.00),
      overflow: 'hidden',
  },
  topContainer: {
    flexDirection: 'row',
    marginTop: 50,
    justifyContent: 'space-evenly',
    alignItems: 'baseline',
    flex: 1,
    width: "100%",
  },
  btnContainer: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    paddingRight: 30,
    paddingLeft: 30
  },
  textStyle: {
    color: '#3681cc',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16
  },

});
