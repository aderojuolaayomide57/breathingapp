import React from 'react';
import {
   StyleSheet,
   View,
   TouchableOpacity,
   Text,
   ScrollView,
} from 'react-native';
import CustomSelect from '../../components/select'
import { Formik } from 'formik';
import * as Yup from 'yup';


const validationSchema = Yup.object().shape({
    inhale: Yup.number()
      .label('inhale'),
    exhale: Yup.number()
        .label('exhale'),
    delay_top: Yup.number()
        .label('Hold'),
    delay_bottom: Yup.number()
        .label('Hold'),
    overall_time: Yup.number()
        .label('Hold')
})



onValueChange = value => {
    const { field, form } = this.props
    form.setFieldValue(field.name, value)
  }



const Ellipse = (props) => {
    const { navigation: { navigate } } = props;
    const initialValues = {
        inhale: 0,
        exhale: 0,
        delay_top: 0,
        delay_bottom: 0,
        overall_time: 0,
    }
        return (
            <ScrollView>
                <Formik
                initialValues={initialValues}
                onSubmit={values => {
                    navigate('EllipseTimer', {values})
                }}
                validationSchema={validationSchema}
                >
                    {formikProps => (
                        <React.Fragment>
                
                            <View style={styles.container}>
                                <CustomSelect
                                    name="inhale"
                                    label="Inhale"
                                    onValueChange={itemValue => formikProps.setFieldValue('inhale', itemValue)}
                                    value={formikProps.values.inhale}

                                />
                                <CustomSelect
                                    name="delay_top"
                                    label="Hold"
                                    value={formikProps.values.delay_top}
                                    onValueChange={itemValue => formikProps.setFieldValue('delay_top', itemValue)}

                                />
                                <CustomSelect
                                    name="exhale"
                                    label="Exhale"
                                    onValueChange={itemValue => formikProps.setFieldValue('exhale', itemValue)}
                                    value={formikProps.values.exhale}
                                />
                                <CustomSelect
                                    name="delay_bottom"
                                    label="Hold"
                                    onValueChange={itemValue => formikProps.setFieldValue('delay_bottom', itemValue)}
                                    value={formikProps.values.delay_bottom}
                                />
                                <CustomSelect
                                    name="overall_time"
                                    label="Animation Duration"
                                    onValueChange={itemValue => formikProps.setFieldValue('overall_time', itemValue)}
                                    value={formikProps.values.overall_time}
                                />
                                <TouchableOpacity 
                                    style={styles.btnContainer}
                                    onPress={formikProps.submitForm}
                                >
                                    <Text style={styles.textStyle}>Start</Text>
                                </TouchableOpacity>
                
                            </View>
                        </React.Fragment>
                    )}
                </Formik>
            </ScrollView>    
        )
}

export default Ellipse


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
    btnContainer: {
        backgroundColor: '#3681cc',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        marginTop: 40
      },
      textStyle: {
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 16
      },
})