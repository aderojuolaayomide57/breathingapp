import React, { Component } from 'react';
import { View } from 'react-native';
import ParticlesBg from 'react-native-particles-bg'

class StressReleaseCircle extends Component {

  render () {
      var color = ['red', 'green', 'blue', '#ff0000', 'purple', 'white']
      return (
        <View style={{width: "100%", height :700, flex: 1}}>
            <ParticlesBg color={color} num={20} type="ball" bg={true} />
        </View>
      );
  }
}

export default StressReleaseCircle;