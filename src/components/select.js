import React from 'react';
import { Picker, View, StyleSheet, Text } from 'react-native';
import { Platform } from 'react-native'


const platform = Platform.OS === 'ios'


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: platform ? 15 : 30,
        width: "100%"
    },
    picker: {
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '900',
        lineHeight: 15,
        letterSpacing: 0.4,
        color: 'grey',
    },
    iosWrapper: {
        width: 70,
        height: 110,
        top: -100,
        paddingBottom: 30,
        paddingTop: 20,

    },
    inputWrapper: {
        width: 120,
        borderColor: 'grey',
        borderWidth: 2,
        borderRadius: 10,
        padding: 5
    },
    labelWrapper: {
        padding: 4,
        alignSelf: 'flex-start',
        left: 15,
        top: -10,
        //elevation: 0.1
    },
    label: {
        paddingTop: 20,
        textAlign: 'center',
        opacity: 0.7,
        color: 'grey',
        top: 5
    }
})




const CustomSelect = props => {
    const start = 0
    return (
        <View style={styles.container}>
            <Text style={styles.label}>{props.label}</Text>
            <View style={platform ? styles.iosWrapper : styles.inputWrapper}>
                <Picker
                    style={styles.picker}
                    onValueChange={props.onValueChange}
                    selectedValue={props.value}
                >
                    {
                        
                        new Array(21)
                        .fill(0)
                        .map((_, i) => {
                            const value = start + i;
                        return (
                                <Picker.Item key={value} label={`${value}`} value={value} />
                            )                    
                        })
                    }
                </Picker>
            </View>
        </View>
    )
}




export default CustomSelect;
