/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

  import React from 'react';

  import { NavigationContainer } from "@react-navigation/native";
  
  import { createStackNavigator } from "@react-navigation/stack";
  import { navigationRef } from './utils/navigation';
  
  
  import Ellipse from "./screens/Ellipse";
  import Home from './screens/Home';
  import Particles from './screens/Particles';
  import WaterWave from './screens/WaterWave';
  import EllipseTimer from './screens/Ellipse/elllipse';
  import StressRelease from './screens/StressRelease';
  import StressReleaseCircle from './screens/StressReleaseCircle';
  import Circle from './screens/Circle';
  
  
  
  const Stack = createStackNavigator();
  
  const screenOptionStyle = {
    headerStyle: {
      backgroundColor: "white",
    },
    headerTintColor: "#3681cc",
    headerBackTitle: "Back",
  };
  
  
  const App = () => {
    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator initialRouteName="Splash" screenOptions={screenOptionStyle}>
          <Stack.Screen 
                name="Home" 
                component={Home} 
                options={{
                  headerShown: false
                }}
            />
          <Stack.Screen 
            name="Ellipse" 
            component={Ellipse} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="Particles" 
            component={Particles} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="WaterWave" 
            component={WaterWave} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="EllipseTimer" 
            component={EllipseTimer} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="StressRelease" 
            component={StressRelease} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="StressReleaseCircle" 
            component={StressReleaseCircle} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="Circle" 
            component={Circle} 
            options={{
              headerShown: true
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  };
  
  export default App;
  